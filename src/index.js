const http = require("http");
const cors = require("cors");
const express = require("express");
const bp = require("body-parser");
const routes = require("./api/routes");
const cookieParser = require("cookie-parser");

const app = express();
const port = 8888;

app.get("/status", (req, res) => {
  res.status(200).send("up");
});

app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Load routes
app.use("/api", routes);

const server = http.createServer(app);
server.listen(port, () => console.log("Server is running"));
