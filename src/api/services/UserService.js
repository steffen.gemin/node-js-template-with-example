const { UsersTable } = require("../repositories");
const bcrypt = require("bcrypt");
const { sign } = require("jsonwebtoken");
require("dotenv").config();

const createNewUser = async (name, email, password) => {
  try {
    const usersWithEmail = await UsersTable.getUserByEmail(email);
    if (usersWithEmail) {
      throw new Error("Email has already been used!");
    }

    const date = new Date().toISOString().replace(/T/, " ").replace(/\..+/, "");
    const hashedPass = await bcrypt.hashSync(password, 12);

    return await UsersTable.newUser(name, email, hashedPass, date, date);
  } catch (e) {
    throw new Error(e.message);
  }
};

const validateUserLogin = async (email, password) => {
  try {
    const user = await UsersTable.getUserByEmail(email);
    if (!user) {
      throw new Error("could not find user");
    }

    const valid = await bcrypt.compare(password, user.password);

    if (!valid) {
      throw new Error("incorrect password");
    }

    // login succesful

    return {
      accessToken: createNewAccesToken(user),
      refreshToken: createRefreshToken(user),
    };
  } catch (e) {
    throw new Error(e.message);
  }
};

const getUserById = async (id) => {
  try {
    return (user = await UsersTable.getUserById(id));
  } catch (e) {
    throw new Error(e.message);
  }
};

const createNewAccesToken = (user) => {
  return sign({ userId: user.id }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "15m",
  });
};

const createRefreshToken = (user) => {
  return sign({ userId: user.id }, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: "30d",
  });
};

module.exports = {
  createNewUser,
  validateUserLogin,
  getUserById,
  createNewAccesToken,
  createRefreshToken,
};
