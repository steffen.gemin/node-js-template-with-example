const toUser = (item) => {
  return {
    id: item.id,
    name: item.name,
    email: item.email,
  };
};

module.exports = {
  toUser,
};
