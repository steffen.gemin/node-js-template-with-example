const { verify } = require("jsonwebtoken");
const { toUser } = require("../models/UserModel");
const { UserService } = require("../services");

const registerNewUser = async (req, res) => {
  const body = req.body;
  try {
    const createdUser = await UserService.createNewUser(
      body.name,
      body.email,
      body.password
    );

    res.status(201).json(createdUser);
  } catch (e) {
    console.log(e.message);
    res.status(400).end("Error: " + e.message);
  }
};

const loginUser = async (req, res) => {
  const body = req.body;
  try {
    const tokens = await UserService.validateUserLogin(
      body.email,
      body.password
    );

    sendRefreshToken(res, tokens.refreshToken);

    res.status(201).json({
      accessToken: tokens.accessToken,
    });
  } catch (e) {
    console.log(e.message);
    res.status(400).end("Error: " + e.message);
  }
};

const getLoggedInUser = async (req, res) => {
  try {
    const user = await UserService.getUserById(req.user.userId);

    res.status(201).json(toUser(user));
  } catch (e) {
    console.log("e" + e.message);
    res.status(400).end("Error: " + e.message);
  }
};

const refreshToken = async (req, res) => {
  const token = req.cookies.jid;
  if (!token) {
    throw new Error("No token");
  }
  try {
    const payload = verify(token, process.env.REFRESH_TOKEN_SECRET);

    const user = await UserService.getUserById(payload.userId);

    if (!user) {
      throw new Error("No user found");
    }

    sendRefreshToken(res, UserService.createRefreshToken(user));

    res.send({
      ok: true,
      accessToken: await UserService.createNewAccesToken(user),
    });
  } catch (e) {
    console.log(e.message);
    res.send({ ok: false, accessToken: "" });
  }
};

const sendRefreshToken = (res, token) => {
  res.cookie("jid", token, {
    httpOnly: true,
  });
};
module.exports = {
  registerNewUser,
  loginUser,
  getLoggedInUser,
  refreshToken,
};
