const BlogPostController = require("./BlogPostsController")
const UserController = require("./UserController")

module.exports = {
    BlogPostController,
    UserController
}