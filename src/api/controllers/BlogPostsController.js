const { BlogPostsServices } = require('../services')

const getBlogPosts = async (req, res) => {
    try{
        const posts = await BlogPostsServices.getBlogPosts();
        res.status(200).json(posts)
    }catch(e){
        console.log(e.message)
        res.sendStatus(500)
    }
    
}

module.exports = {
    getBlogPosts
}
