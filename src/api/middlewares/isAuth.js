const { verify } = require("jsonwebtoken");

module.exports = function (req, res, next) {
  try {
    const authorization = req.headers["authorization"];

    if (!authorization) {
      throw new Error("Invalid bearer token");
    }

    const token = authorization.split(" ")[1];
    const payload = verify(token, process.env.ACCESS_TOKEN_SECRET);
    req.user = payload;
    next();
  } catch (e) {
    res.status(400).send("Invalid bearer token");
  }
};
