
const MySQLDatabase = require("./MySQL"); 
const MongoDBDatabase = null;

const serviceClasses = {
    "MySQL": MySQLDatabase,
    "MongoDB": MongoDBDatabase,
};
  
//Check env to initialize 
const type = "MySQL"

activeServiceClass = serviceClasses[type]

module.exports = activeServiceClass;