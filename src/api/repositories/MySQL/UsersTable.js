const db = require("./MySQLConnection");

const newUser = async (
  name,
  email,
  password,
  created_at,
  updated_at,
  email_verified_at = null,
  remember_token = null
) => {
  try {
    const SQL =
      "INSERT INTO `users`(`name`, `email`, `password`, `created_at`, `updated_at`, `email_verified_at`, `remember_token`) VALUES(?, ?, ?, ?, ?, ?, ?);";
    const result = await db.execute(SQL, [
      name,
      email,
      password,
      created_at,
      updated_at,
      email_verified_at,
      remember_token,
    ]);
    return result;
  } catch (e) {
    throw new Error(e.message);
  }
};

const getUserByEmail = async (email) => {
  try {
    const SQL = "select * from `users` where email = ? limit 1;";
    const result = await db.execute(SQL, [email]);
    return result[0];
  } catch (e) {
    throw new Error(e.message);
  }
};

const getUserById = async (id) => {
  try {
    const SQL = "select * from `users` where id = ?;";
    const result = await db.execute(SQL, [id]);
    return result[0];
  } catch (e) {
    throw new Error(e.message);
  }
};

module.exports = {
  newUser,
  getUserByEmail,
  getUserById,
};
