const database_config = {
    "host": "localhost",
    "port": "3306",
    "database": "test_schema",
    "user": "root",
    "password": "root"
} //TODO to env


const mysql = require('mysql');

console.log(database_config)

exports.execute = (query, args = []) => {
    return new Promise((resolve, reject) => {
        const connection = mysql.createConnection(database_config)

        return connection.connect((err) => {
            if (err) reject(err);
            return connection.query(query, args, (err, rows) => {
                connection.end();
                if (err) reject(err);
                else resolve(rows);
            })
        })
    })

}