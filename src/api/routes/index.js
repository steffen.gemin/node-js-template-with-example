const express = require('express')
const rootRouter = express.Router()


const blogRoutes = require('./BlogPostsRoutes');
const userRoutes = require('./UserRoutes');


rootRouter.use('/blog', blogRoutes);
rootRouter.use('/auth', userRoutes);

module.exports = rootRouter;