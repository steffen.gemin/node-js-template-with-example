const express = require('express')

const { BlogPostController } = require('../controllers')

const router = express.Router()

router.get("/", BlogPostController.getBlogPosts);



module.exports = router