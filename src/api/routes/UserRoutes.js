const express = require("express");

const { UserController } = require("../controllers");
const isAuth = require("../middlewares/isAuth");

const router = express.Router();

router.post("/register", UserController.registerNewUser);

router.post("/login", UserController.loginUser);

router.post("/refresh_token", UserController.refreshToken);

router.get("/me", isAuth, UserController.getLoggedInUser);

module.exports = router;
